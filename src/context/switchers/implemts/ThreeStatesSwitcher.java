package context.switchers.implemts;

import context.Context;
import context.implement.subcontexts.CupContext;
import state.State;
import state.implemts.substates.EmptyState;
import state.implemts.substates.FilledState;
import state.implemts.substates.MedianState;

public class ThreeStatesSwitcher extends BaseSwitcher
{
    public ThreeStatesSwitcher(Context context)
    {
        super(context);
        this.intiStates();
    }


    @Override
    public State switchState()
    {
        float amount = this.getAmount();
        if (amount == 0) {
            return this.getState(0);
        }
        else if (amount == 1) {
            return this.getState(2);
        }
        else {
            return this.getState(1);
        }
    }

    private float getAmount()
    {
        int maxAmount = this.getContext().getMaxAmount();
        int currentAmount = this.getContext().getCurrentAmount();

        return (float) currentAmount/maxAmount;
    }

    @Override
    protected void intiStates()
    {
        this.addState(new EmptyState());
        this.addState(new MedianState());
        this.addState(new FilledState());
    }

    @Override
    protected CupContext getContext()
    {
        return (CupContext) super.getContext();
    }
}
