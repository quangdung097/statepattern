package context.switchers.implemts;

import context.Context;
import context.switchers.Switchable;
import state.State;

abstract public class BaseSwitcher implements Switchable
{
    private State[] states;
    private int statesIndex;
    private Context context;

    private static final int MAX_STATE_COUNT = 10;

    protected BaseSwitcher(Context context)
    {
        this.states = new State[MAX_STATE_COUNT];
        this.statesIndex = 0;
        this.context = context;
    }

    abstract protected void intiStates();

    protected void addState(State state)
    {
        this.states[this.statesIndex] = state;
        this.statesIndex ++;
    }

    protected Context getContext()
    {
        return  this.context;
    }

    protected State getState(int index)
    {
        if (index < MAX_STATE_COUNT) {
            return this.states[index];
        }
        return null;
    }
}
