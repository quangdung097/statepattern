package context.switchers;

import state.State;

public interface Switchable
{
    State switchState();
}
