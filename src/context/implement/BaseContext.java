package context.implement;

import context.Context;
import state.State;

/**
 * State value, that be delegated by Context to do the execute method
 */
public class BaseContext implements Context
{
    private State state;

    public BaseContext(State initialState)
    {
        this.state = initialState;
    }

    @Override
    public void execute()
    {
        this.state.execute();
    }

    @Override
    public void switchState(State state)
    {
        this.state = state;
    }
}
