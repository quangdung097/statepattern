package context.implement.subcontexts;

import context.switchers.Switchable;
import context.switchers.implemts.ThreeStatesSwitcher;
import state.State;

public class MaxTwoCupContext extends CupContext
{
    private static final int MAX_AMOUNT = 2;

    public MaxTwoCupContext(State initialState)
    {
        super(initialState);
    }

    @Override
    int createMaxAmount()
    {
        return MaxTwoCupContext.MAX_AMOUNT;
    }

    @Override
    Switchable createSwitcher()
    {
        return new ThreeStatesSwitcher(this);
    }
}
