package context.implement.subcontexts;

import context.Cup;
import context.implement.BaseContext;
import context.switchers.Switchable;
import state.State;

abstract public class CupContext extends BaseContext implements Cup
{
    private int maxAmount;
    private int currentAmount;
    private Switchable switcher;

    CupContext(State initialState)
    {
        super(initialState);
        this.currentAmount = 0;
        this.maxAmount = this.createMaxAmount();
        this.switcher = this.createSwitcher();
    }

    @Override
    public boolean fill()
    {
        if (this.currentAmount < this.maxAmount) {
            this.currentAmount += 1;
            this.switchState(this.switcher.switchState());
            return true;
        }
        return false;
    }

    @Override
    public boolean pour()
    {
        if (this.currentAmount > 0) {
            this.currentAmount -= 1;
            this.switchState(this.switcher.switchState());
            return true;
        }
        return false;
    }

    @Override
    public int getCurrentAmount()
    {
        return this.currentAmount;
    }

    @Override
    public int getMaxAmount()
    {
        return this.maxAmount;
    }

    abstract int createMaxAmount();

    abstract Switchable createSwitcher();
}
