package context;

public interface Cup
{
    boolean fill();

    boolean pour();

    int getMaxAmount();

    int getCurrentAmount();
}
