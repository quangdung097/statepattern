package context;

import state.State;

public interface Context
{
    void execute();

    void switchState(State state);
}
