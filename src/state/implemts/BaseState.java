package state.implemts;

import context.Context;
import state.State;

abstract public class BaseState implements State
{
    private String message;
    private Context context;

    protected BaseState()
    {
        this.context = null;
        this.message = this.createStateMessage();
    }

    @Override
    public void execute()
    {
        System.out.println("State message: " + this.message);
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    @Override
    public void switchState(State state)
    {
        this.context.switchState(state);
    }

    abstract protected String createStateMessage();
}
