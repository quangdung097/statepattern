package state.implemts.substates;

import state.implemts.BaseState;

/**
 * State exists when the cup is half-filed
 */
public class MedianState extends BaseState
{
    private static final String MESSAGE  = "It's half-filled";

    @Override
    protected String createStateMessage()
    {
        return MedianState.MESSAGE;
    }
}
