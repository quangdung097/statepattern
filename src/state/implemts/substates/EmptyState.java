package state.implemts.substates;

import state.implemts.BaseState;

/**
 * State exists when the cup is empty
 */
public class EmptyState extends BaseState
{
    private static final String MESSAGE  = "It's empty";

    @Override
    protected String createStateMessage()
    {
        return EmptyState.MESSAGE;
    }
}
