package state.implemts.substates;

import state.implemts.BaseState;

/**
 * State exists when the cup is filled
 */
public class FilledState extends BaseState
{
    private static final String MESSAGE  = "It's filled";

    @Override
    protected String createStateMessage()
    {
        return FilledState.MESSAGE;
    }
}
