package state;

public interface State
{
    /**
     * Function that varies depend on each states
     */
    void execute();

    /**
     * Switch state between each states
     * @param state
     */
    void switchState(State state);
}
