import context.Context;
import context.Cup;
import context.implement.subcontexts.MaxTwoCupContext;
import state.State;
import state.implemts.substates.EmptyState;

public class Main
{
    public static void main(String[] args)
    {
        //init state for cup
        State emptyState = new EmptyState();
        Context cup = new MaxTwoCupContext(emptyState);

        //print current state
        cup.execute();

        Cup baseCup = (Cup) cup;

        //fill in the cup and print current state
        baseCup.fill();
        cup.execute();

        //fill in the cup and print current state
        baseCup.fill();
        cup.execute();

        //pour out and print current state
        baseCup.pour();
        cup.execute();
    }
}
